# Hierarchical Depression Symptom Classifier

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.12657260.svg)](https://doi.org/10.5281/zenodo.12657260)

This repository contains the implementation of the model used in [Towards Automatic Text-Based Estimation of Depression through Symptom Prediction](https://www.researchsquare.com/article/rs-2096670/v1).

Before you can run the model, you have to obtain the DAIC-WOZ corpus. Please, refer to the **Availability of Data and Materials** section for the instructions.

### Availability of Data and Materials
The DAIC-WOZ dataset that supports the findings of this study is available from The University of Southern California Institute for Creative Technologies (https://dcapswoz.ict.usc.edu/) but restrictions apply to the availability of these data, which were used under license for the current study, and so are not publicly available. Dataset is however available from the University of Southern California Institute for Creative Technologies upon reasonable request.

### Data

Once you have the data, it has to be organized in the following structure:

    .
    ├── ...
    ├── data
    │   ├── transcripts
    │   │   ├── 300_TRANSCRIPT.csv
    │   │   ├── 301_TRANSCRIPT.csv 
    │   │   └── ...
    │   ├── train_split_Depression_AVEC2017.csv
    │   ├── dev_split_Depression_AVEC2017.csv
    │   └── test_split_Depression_AVEC2017.csv
    └── ...

After that, you have to run `prepare_data.py`.

### Training the model

Training is done with [HuggingFace 🤗 Accelerate](https://huggingface.co/docs/accelerate/index) library.

Before training, run `accelerate config` to setup your environment.

Set the training parameters in the `config.py` file.

To start training, run:

```
accelerate launch train.py
```

### Testing the model

Refer to `test_model.py` for more information.

### How to cite this material

DOI: 10.5281/zenodo.12657260